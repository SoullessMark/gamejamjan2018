﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehabiour : MonoBehaviour {

    public float thrust;
    public GameObject current_active_object;
    public bool is_power_active = false;
    private Rigidbody current_active_body;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            CheckForCast();
        }
        if (is_power_active)
        {
            UseMovePower();
        }
	}


    public void CheckForCast()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Moveable")
            {
                current_active_object = hit.collider.gameObject;
                current_active_body = current_active_object.gameObject.GetComponent<Rigidbody>();
                is_power_active = true;
            }
        }
    }

    public void UseMovePower()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {

            current_active_body.AddForce(transform.right * thrust);
        }else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            current_active_body.AddForce(transform.right * (thrust * -1));
        }else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            current_active_body.AddForce(transform.forward * thrust);
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            current_active_body.AddForce(transform.forward * (thrust * -1));
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            current_active_body.AddForce(transform.up * thrust);
        }
    }
}
